import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DataProvider');
  }

  obtenerPlaces() {
    return this.http.get('http://myvacations.local:8080/api/home');
  }

  obtenerItems() {
    return this.http.get('http://myvacations.local:8080/api/visits/1');
  }

}
