import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { DataProvider } from '../../providers/data/data';


@Component({
  selector: 'page-item-details',
  templateUrl: 'list.html'
})
export class ListPage {
list
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public items: DataProvider) {}

  ionViewDidLoad() {
    this.items.obtenerItems()
    .subscribe(
      (data) => {this.list = data;},
      (err) => {console.log(err);}
    )
  }
  
}