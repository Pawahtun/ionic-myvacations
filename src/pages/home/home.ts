import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ListPage } from '../list/list';

import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
list
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public items: DataProvider) {}

  ionViewDidLoad() {
    this.items.obtenerPlaces()
    .subscribe(
      (data) => {this.list = data;},
      (err) => {console.log(err);}
    )
  }

  goSub(event, item) {
    this.navCtrl.push(ListPage, {
      item: item
    });
  }
}
